# Translation of kcmnic into Japanese.
# This file is distributed under the same license as the kdebase package.
# Taiki Komoda <kom@kde.gr.jp>, 2004.
# Fumiaki Okushi <fumiaki@okushi.com>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmnic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:14+0000\n"
"PO-Revision-Date: 2007-06-24 23:00+0900\n"
"Last-Translator: Yukiko Bando <ybando@k6.dion.ne.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.1\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: networkmodel.cpp:161
#, fuzzy, kde-format
#| msgid "Point to Point"
msgctxt "@item:intable Network type"
msgid "Point to Point"
msgstr "ポイント to ポイント"

#: networkmodel.cpp:168
#, fuzzy, kde-format
#| msgctxt "@item:intext Mode of network card"
#| msgid "Broadcast"
msgctxt "@item:intable Network type"
msgid "Broadcast"
msgstr "ブロードキャスト"

#: networkmodel.cpp:175
#, fuzzy, kde-format
#| msgid "Multicast"
msgctxt "@item:intable Network type"
msgid "Multicast"
msgstr "マルチキャスト"

#: networkmodel.cpp:182
#, fuzzy, kde-format
#| msgid "Loopback"
msgctxt "@item:intable Network type"
msgid "Loopback"
msgstr "ループバック"

#: ui/main.qml:29
#, fuzzy, kde-format
#| msgid "Name"
msgctxt "@label"
msgid "Name:"
msgstr "名前"

#: ui/main.qml:33
#, fuzzy, kde-format
#| msgid "IP Address"
msgctxt "@label"
msgid "Address:"
msgstr "IP アドレス"

#: ui/main.qml:37
#, fuzzy, kde-format
#| msgid "Network Mask"
msgctxt "@label"
msgid "Network Mask:"
msgstr "ネットワークマスク"

#: ui/main.qml:41
#, fuzzy, kde-format
#| msgid "Type"
msgctxt "@label"
msgid "Type:"
msgstr "タイプ"

#: ui/main.qml:45
#, kde-format
msgctxt "@label"
msgid "Hardware Address:"
msgstr ""

#: ui/main.qml:50
#, fuzzy, kde-format
#| msgid "State"
msgctxt "@label"
msgid "State:"
msgstr "状態"

#: ui/main.qml:58
#, kde-format
msgctxt "State of network card is connected"
msgid "Up"
msgstr "接続中"

#: ui/main.qml:59
#, kde-format
msgctxt "State of network card is disconnected"
msgid "Down"
msgstr "切断中"

#: ui/main.qml:70
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr ""

#, fuzzy
#~| msgid "Network Mask"
#~ msgid "Network Interfaces"
#~ msgstr "ネットワークマスク"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Noboru Sinohara"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "shinobo@leo.bekkoame.ne.jp"

#, fuzzy
#~| msgid "Network Mask"
#~ msgctxt "@title:window"
#~ msgid "Network Interfaces"
#~ msgstr "ネットワークマスク"

#, fuzzy
#~| msgid "(c) 2001 - 2002 Alexander Neundorf"
#~ msgctxt "@info"
#~ msgid "(c) 2001 - 2002 Alexander Neundorf"
#~ msgstr "(c) 2001 - 2002 Alexander Neundorf"

#, fuzzy
#~| msgid "Alexander Neundorf"
#~ msgctxt "@info:credit"
#~ msgid "Alexander Neundorf"
#~ msgstr "Alexander Neundorf"

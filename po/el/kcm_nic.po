# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Petros Vidalis <p_vidalis@hotmail.com>, 2010.
# Dimitrios Glentadakis <dglent@gmail.com>, 2011.
# Stelios <sstavra@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:14+0000\n"
"PO-Revision-Date: 2020-09-27 10:08+0300\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: networkmodel.cpp:161
#, kde-format
msgctxt "@item:intable Network type"
msgid "Point to Point"
msgstr "Δισημειακό"

#: networkmodel.cpp:168
#, kde-format
msgctxt "@item:intable Network type"
msgid "Broadcast"
msgstr "Αναμετάδοση"

#: networkmodel.cpp:175
#, kde-format
msgctxt "@item:intable Network type"
msgid "Multicast"
msgstr "Πολλαπλή αναμετάδοση"

#: networkmodel.cpp:182
#, kde-format
msgctxt "@item:intable Network type"
msgid "Loopback"
msgstr "Βρόχος ανακύκλωσης"

#: ui/main.qml:29
#, kde-format
msgctxt "@label"
msgid "Name:"
msgstr "Όνομα:"

#: ui/main.qml:33
#, kde-format
msgctxt "@label"
msgid "Address:"
msgstr "Διεύθυνση:"

#: ui/main.qml:37
#, kde-format
msgctxt "@label"
msgid "Network Mask:"
msgstr "Μάσκα δικτύου:"

#: ui/main.qml:41
#, kde-format
msgctxt "@label"
msgid "Type:"
msgstr "Τύπος:"

#: ui/main.qml:45
#, kde-format
msgctxt "@label"
msgid "Hardware Address:"
msgstr "Διεύθυνση υλικού:"

#: ui/main.qml:50
#, kde-format
msgctxt "@label"
msgid "State:"
msgstr "Κατάσταση:"

#: ui/main.qml:58
#, kde-format
msgctxt "State of network card is connected"
msgid "Up"
msgstr "Πάνω"

#: ui/main.qml:59
#, kde-format
msgctxt "State of network card is disconnected"
msgid "Down"
msgstr "Κάτω"

#: ui/main.qml:70
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr "Ανανέωση"

#~ msgid "Network Interfaces"
#~ msgstr "Δικτυακές διεπαφές"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Δημήτριος Γλενταδάκης"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "dglent@gmail.com"

#~ msgid "kcm_nic"
#~ msgstr "kcm_nic"

#~ msgctxt "@title:window"
#~ msgid "Network Interfaces"
#~ msgstr "Δικτυακή διεπαφή"

#~ msgctxt "@info"
#~ msgid "(c) 2001 - 2002 Alexander Neundorf"
#~ msgstr "(c) 2001 - 2002 Alexander Neundorf"

#~ msgctxt "@info:credit"
#~ msgid "Alexander Neundorf"
#~ msgstr "Alexander Neundorf"

#~ msgctxt "@info:credit"
#~ msgid "creator"
#~ msgstr "δημιουργός"

#~ msgctxt "@info:credit"
#~ msgid "Carl Schwan"
#~ msgstr "Carl Schwan"

#~ msgctxt "@info:credit"
#~ msgid "developer"
#~ msgstr "προγραμματιστής"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Broadcast"
#~ msgstr "Αναμετάδοση"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Point to Point"
#~ msgstr "Δισημειακό"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Multicast"
#~ msgstr "Πολλαπλή αναμετάδοση"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Loopback"
#~ msgstr "Βρόχος ανακύκλωσης"

#~ msgctxt "@item:intext Mode of network card"
#~ msgid "Unknown"
#~ msgstr "Άγνωστο"

#~ msgctxt "Unknown network mask"
#~ msgid "Unknown"
#~ msgstr "Άγνωστο"

#~ msgctxt "Unknown HWaddr"
#~ msgid "Unknown"
#~ msgstr "Άγνωστο"

#, fuzzy
#~| msgid "Broadcast"
#~ msgctxt "@item:intable Netork type"
#~ msgid "Broadcast"
#~ msgstr "Εκπομπή"

#~ msgid "HWAddr"
#~ msgstr "HWAddr"

#~ msgid "&Update"
#~ msgstr "Ενη&μέρωση"

#~ msgid "kcminfo"
#~ msgstr "kcminfo"

#~ msgid "System Information Control Module"
#~ msgstr "Άρθρωμα ελέγχου πληροφοριών συστήματος"

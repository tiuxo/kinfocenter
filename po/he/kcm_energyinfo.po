# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: kcm_energyinfo\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-16 02:03+0000\n"
"PO-Revision-Date: 2017-05-16 11:23-0400\n"
"Last-Translator: Elkana Bardugo <ttv200@gmail.com>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Zanata 3.9.6\n"

#: ui/main.qml:45
#, kde-format
msgid "Battery"
msgstr "סוללה"

#: ui/main.qml:47
#, kde-format
msgid "Rechargeable"
msgstr "נטען"

#: ui/main.qml:48
#, kde-format
msgid "Charge state"
msgstr "מצב טעינה"

#: ui/main.qml:49
#, fuzzy, kde-format
#| msgid "Current"
msgid "Current charge"
msgstr "נוכחי"

#: ui/main.qml:49 ui/main.qml:197
#, fuzzy, kde-format
#| msgctxt "%1 is value, %2 is unit"
#| msgid "%1 %2"
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1 %2"

#: ui/main.qml:50
#, kde-format
msgid "Health"
msgstr ""

#: ui/main.qml:50
#, fuzzy, kde-format
#| msgctxt "%1 is value, %2 is unit"
#| msgid "%1 %2"
msgctxt "Battery health percentage"
msgid "%1%"
msgstr "%1 %2"

#: ui/main.qml:51
#, kde-format
msgid "Vendor"
msgstr "ספק"

#: ui/main.qml:52
#, kde-format
msgid "Model"
msgstr "דגם"

#: ui/main.qml:53
#, kde-format
msgid "Serial Number"
msgstr "מספר סידורי"

#: ui/main.qml:54
#, kde-format
msgid "Technology"
msgstr ""

#: ui/main.qml:58
#, kde-format
msgid "Energy"
msgstr "אנרגיה"

#: ui/main.qml:60
#, kde-format
msgctxt "current power draw from the battery in W"
msgid "Consumption"
msgstr "צריכה"

#: ui/main.qml:60
#, kde-format
msgctxt "Watt"
msgid "W"
msgstr "וואט"

#: ui/main.qml:61
#, kde-format
msgid "Voltage"
msgstr "וולט"

#: ui/main.qml:61
#, kde-format
msgctxt "Volt"
msgid "V"
msgstr "V"

#: ui/main.qml:62
#, kde-format
msgid "Remaining energy"
msgstr ""

#: ui/main.qml:62 ui/main.qml:63 ui/main.qml:64
#, kde-format
msgctxt "Watt-hours"
msgid "Wh"
msgstr "Wh"

#: ui/main.qml:63
#, fuzzy, kde-format
#| msgid "Last full"
msgid "Last full charge"
msgstr "קיבולת מלאה אחרונה"

#: ui/main.qml:64
#, kde-format
msgid "Original charge capacity"
msgstr ""

#: ui/main.qml:68
#, kde-format
msgid "Environment"
msgstr "סביבה"

#: ui/main.qml:70
#, kde-format
msgid "Temperature"
msgstr "טמפרטורה"

#: ui/main.qml:70
#, kde-format
msgctxt "Degree Celsius"
msgid "°C"
msgstr "מעלות צלזיוס"

#: ui/main.qml:77
#, kde-format
msgid "Not charging"
msgstr "לא נטען"

#: ui/main.qml:78
#, kde-format
msgid "Charging"
msgstr "נטען"

#: ui/main.qml:79
#, kde-format
msgid "Discharging"
msgstr "לא מחובר לחשמל"

#: ui/main.qml:80
#, kde-format
msgid "Fully charged"
msgstr "טעון במלואו"

#: ui/main.qml:86
#, kde-format
msgid "Lithium ion"
msgstr ""

#: ui/main.qml:87
#, kde-format
msgid "Lithium polymer"
msgstr ""

#: ui/main.qml:88
#, kde-format
msgid "Lithium iron phosphate"
msgstr ""

#: ui/main.qml:89
#, kde-format
msgid "Lead acid"
msgstr ""

#: ui/main.qml:90
#, kde-format
msgid "Nickel cadmium"
msgstr ""

#: ui/main.qml:91
#, kde-format
msgid "Nickel metal hydride"
msgstr ""

#: ui/main.qml:92
#, kde-format
msgid "Unknown technology"
msgstr ""

#: ui/main.qml:99
#, kde-format
msgid "Last hour"
msgstr "שעה אחרונה"

#: ui/main.qml:99
#, kde-format
msgid "Last 2 hours"
msgstr "שעתיים אחרונות"

#: ui/main.qml:99
#, kde-format
msgid "Last 12 hours"
msgstr "12 שעות אחרונות"

#: ui/main.qml:99
#, kde-format
msgid "Last 24 hours"
msgstr "24 שעות אחרונות"

#: ui/main.qml:99
#, kde-format
msgid "Last 48 hours"
msgstr "48 שעות אחרונות"

#: ui/main.qml:99
#, kde-format
msgid "Last 7 days"
msgstr "7 ימים אחרונים"

#: ui/main.qml:106
#, kde-format
msgctxt "@info:status"
msgid "No energy information available on this system"
msgstr ""

#: ui/main.qml:171
#, kde-format
msgid "Internal battery"
msgstr ""

#: ui/main.qml:172
#, fuzzy, kde-format
#| msgid "Battery"
msgid "UPS battery"
msgstr "סוללה"

#: ui/main.qml:173
#, kde-format
msgid "Monitor battery"
msgstr ""

#: ui/main.qml:174
#, fuzzy, kde-format
#| msgid "Battery"
msgid "Mouse battery"
msgstr "סוללה"

#: ui/main.qml:175
#, kde-format
msgid "Keyboard battery"
msgstr ""

#: ui/main.qml:176
#, fuzzy, kde-format
#| msgid "Battery"
msgid "PDA battery"
msgstr "סוללה"

#: ui/main.qml:177
#, fuzzy, kde-format
#| msgid "Battery"
msgid "Phone battery"
msgstr "סוללה"

#: ui/main.qml:178
#, kde-format
msgid "Unknown battery"
msgstr ""

#: ui/main.qml:196
#, fuzzy, kde-format
#| msgid "Charging"
msgctxt "Battery charge percentage"
msgid "%1% (Charging)"
msgstr "נטען"

#: ui/main.qml:246
#, kde-format
msgctxt "Shorthand for Watts"
msgid "W"
msgstr "וואט"

#: ui/main.qml:246
#, kde-format
msgctxt "literal percent sign"
msgid "%"
msgstr "%"

#: ui/main.qml:272
#, kde-format
msgid "Charge Percentage"
msgstr "אחוז טעינה"

#: ui/main.qml:282
#, kde-format
msgid "Energy Consumption"
msgstr "צריכת אנרגיה"

#: ui/main.qml:297
#, kde-format
msgid "Timespan"
msgstr "טווח זמן"

#: ui/main.qml:298
#, kde-format
msgid "Timespan of data to display"
msgstr "טווח זמן להצגת נתונים"

#: ui/main.qml:304
#, kde-format
msgid "Refresh"
msgstr "רענן"

#: ui/main.qml:316
#, kde-format
msgid "This type of history is currently not available for this device."
msgstr "סוג נתונים זה אינו זמין להתקן זה כרגע"

#: ui/main.qml:374
#, kde-format
msgid "%1:"
msgstr "%1:"

#: ui/main.qml:385
#, kde-format
msgid "Yes"
msgstr "כן"

#: ui/main.qml:387
#, kde-format
msgid "No"
msgstr "לא"

#: ui/main.qml:405
#, kde-format
msgctxt "%1 is value, %2 is unit"
msgid "%1 %2"
msgstr "%1 %2"

#~ msgid "%"
#~ msgstr "%"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "אלקנה ברדוגו"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "ttv200@gmail.com"

#~ msgid "Energy Consumption Statistics"
#~ msgstr "סטטיסטיקת צריכת אנרגיה"

#~ msgid "Kai Uwe Broulik"
#~ msgstr "Kai Uwe Broulik"

#~ msgid "Application Energy Consumption"
#~ msgstr "צריכת סוללה של יישומים"

#~ msgid "Path: %1"
#~ msgstr "נתיב: %1"

#~ msgid "PID: %1"
#~ msgstr "‏‏PID: %1"

#~ msgid "Wakeups per second: %1 (%2%)"
#~ msgstr "התעררויות לשניה: %1 (%2%)"

#~ msgid "Details: %1"
#~ msgstr "פרטים: %1"

#~ msgid "Manufacturer"
#~ msgstr "יצרן"

#~ msgid "Full design"
#~ msgstr "קיבולת מקורית"

#~ msgid "System"
#~ msgstr "מערכת"

#~ msgid "Has power supply"
#~ msgstr "אספקת חשמל קיימת:"

#~ msgid "Capacity"
#~ msgstr "קיבולת"

# Indonesian translations for kcmdevinfo package.
# Copyright (C) 2010 This_file_is_part_of_KDE
# This file is distributed under the same license as the kcmdevinfo package.
# Andhika Padmawan <andhika.padmawan@gmail.com>, 2010-2014.
# Wantoyo <wantoyek@gmail.com>, 2017, 2018, 2019, 2020, 2022.
# Aziz Adam Adrian <4.adam.adrian@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kcmdevinfo\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-19 02:06+0000\n"
"PO-Revision-Date: 2022-08-26 23:44+0700\n"
"Last-Translator: Wantoyèk <wantoyek@gmail.com>\n"
"Language-Team: https://t.me/Localizations_KDE_Indonesia\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: devicelisting.cpp:41
#, kde-format
msgctxt "Device Listing Whats This"
msgid "Shows all the devices that are currently listed."
msgstr "Tampilkan semua peranti yang saat ini terdaftar."

#: devicelisting.cpp:44
#, kde-format
msgid "Devices"
msgstr "Peranti"

#: devicelisting.cpp:57
#, kde-format
msgid "Collapse All"
msgstr "Ciutkan Semua"

#: devicelisting.cpp:60
#, kde-format
msgid "Expand All"
msgstr "Bentangkan Semua"

#: devicelisting.cpp:63
#, kde-format
msgid "Show All Devices"
msgstr "Tampilkan Semua Peranti"

#: devicelisting.cpp:66
#, kde-format
msgid "Show Relevant Devices"
msgstr "Tampilkan Peranti Relevan"

#: devicelisting.cpp:95
#, kde-format
msgctxt "unknown device type"
msgid "Unknown"
msgstr "Tak Diketahui"

#: devicelisting.cpp:136 devinfo.cpp:73
#, kde-format
msgctxt "no device UDI"
msgid "None"
msgstr "Nihil"

#: devinfo.cpp:52
#, kde-format
msgid "UDI: "
msgstr "UDI: "

#: devinfo.cpp:60
#, kde-format
msgctxt "Udi Whats This"
msgid "Shows the current device's UDI (Unique Device Identifier)"
msgstr "Tampilkan UDI (Unique Device Identifier) peranti saat ini"

#: infopanel.cpp:20
#, kde-format
msgid "Device Information"
msgstr "Informasi Peranti"

#: infopanel.cpp:29
#, kde-format
msgctxt "Info Panel Whats This"
msgid "Shows information about the currently selected device."
msgstr "Tampilkan informasi tentang peranti yang saat ini terpilih."

#: infopanel.cpp:56
#, kde-format
msgid ""
"\n"
"Solid Based Device Viewer Module"
msgstr ""
"\n"
"Modul Penampil Peranti Berbasis Solid"

#: infopanel.cpp:119
#, kde-format
msgid "Description: "
msgstr "Deskripsi: "

#: infopanel.cpp:121
#, kde-format
msgid "Product: "
msgstr "Produk: "

#: infopanel.cpp:123
#, kde-format
msgid "Vendor: "
msgstr "Vendor: "

#: infopanel.cpp:145
#, kde-format
msgid "Yes"
msgstr "Ya"

#: infopanel.cpp:147
#, kde-format
msgid "No"
msgstr "Tidak"

#: infopanel.h:36
#, kde-format
msgctxt "name of something is not known"
msgid "Unknown"
msgstr "Tak Diketahui"

#: soldevice.cpp:65
#, kde-format
msgctxt "unknown device"
msgid "Unknown"
msgstr "Tak Diketahui"

#: soldevice.cpp:91
#, kde-format
msgctxt "Default device tooltip"
msgid "A Device"
msgstr "Peranti"

#: soldevicetypes.cpp:43
#, kde-format
msgid "Processors"
msgstr "Prosesor"

#: soldevicetypes.cpp:59
#, kde-format
msgid "Processor %1"
msgstr "Prosesor %1"

#: soldevicetypes.cpp:76
#, kde-format
msgid "Intel MMX"
msgstr "Intel MMX"

#: soldevicetypes.cpp:79
#, kde-format
msgid "Intel SSE"
msgstr "Intel SSE"

#: soldevicetypes.cpp:82
#, kde-format
msgid "Intel SSE2"
msgstr "Intel SSE2"

#: soldevicetypes.cpp:85
#, kde-format
msgid "Intel SSE3"
msgstr "Intel SSE3"

#: soldevicetypes.cpp:88
#, kde-format
msgid "Intel SSSE3"
msgstr "Intel SSSE3"

#: soldevicetypes.cpp:91
#, kde-format
msgid "Intel SSE4.1"
msgstr "Intel SSE4.1"

#: soldevicetypes.cpp:94
#, kde-format
msgid "Intel SSE4.2"
msgstr "Intel SSE4.2"

#: soldevicetypes.cpp:97
#, kde-format
msgid "AMD 3DNow!"
msgstr "AMD 3DNow!"

#: soldevicetypes.cpp:100
#, kde-format
msgid "ATI IVEC"
msgstr "ATI IVEC"

#: soldevicetypes.cpp:103
#, kde-format
msgctxt "no instruction set extensions"
msgid "None"
msgstr "Nihil"

#: soldevicetypes.cpp:106
#, kde-format
msgid "Processor Number: "
msgstr "Nomor Prosesor: "

#: soldevicetypes.cpp:106
#, kde-format
msgid "Max Speed: "
msgstr "Kecepatan Maks: "

#: soldevicetypes.cpp:107
#, kde-format
msgid "Supported Instruction Sets: "
msgstr "Perangkat Instruksi Didukung: "

#: soldevicetypes.cpp:132
#, kde-format
msgid "Storage Drives"
msgstr "Perangkat Penyimpanan"

#: soldevicetypes.cpp:151
#, kde-format
msgid "Hard Disk Drive"
msgstr "Perangkat Hard Disk"

#: soldevicetypes.cpp:154
#, kde-format
msgid "Compact Flash Reader"
msgstr "Pembaca Compact Flash"

#: soldevicetypes.cpp:157
#, kde-format
msgid "Smart Media Reader"
msgstr "Pembaca Media Pintar"

#: soldevicetypes.cpp:160
#, kde-format
msgid "SD/MMC Reader"
msgstr "Pembaca SD/MMC"

#: soldevicetypes.cpp:163
#, kde-format
msgid "Optical Drive"
msgstr "Perangkat Optik"

#: soldevicetypes.cpp:166
#, kde-format
msgid "Memory Stick Reader"
msgstr "Pembaca Memory Stick"

#: soldevicetypes.cpp:169
#, kde-format
msgid "xD Reader"
msgstr "Pembaca xD"

#: soldevicetypes.cpp:172
#, kde-format
msgid "Unknown Drive"
msgstr "Perangkat Tak Diketahui"

#: soldevicetypes.cpp:192
#, kde-format
msgid "IDE"
msgstr "IDE"

#: soldevicetypes.cpp:195
#, kde-format
msgid "USB"
msgstr "USB"

#: soldevicetypes.cpp:198
#, kde-format
msgid "IEEE1394"
msgstr "IEEE1394"

#: soldevicetypes.cpp:201
#, kde-format
msgid "SCSI"
msgstr "SCSI"

#: soldevicetypes.cpp:204
#, kde-format
msgid "SATA"
msgstr "SATA"

#: soldevicetypes.cpp:207
#, kde-format
msgctxt "platform storage bus"
msgid "Platform"
msgstr "Platform"

#: soldevicetypes.cpp:210
#, kde-format
msgctxt "unknown storage bus"
msgid "Unknown"
msgstr "Tak Diketahui"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Bus: "
msgstr "Bus: "

#: soldevicetypes.cpp:213
#, kde-format
msgid "Hotpluggable?"
msgstr "Dapat Dicabut-colok?"

#: soldevicetypes.cpp:213
#, kde-format
msgid "Removable?"
msgstr "Dapat Dilepas?"

#: soldevicetypes.cpp:257
#, kde-format
msgid "Unused"
msgstr "Tak Digunakan"

#: soldevicetypes.cpp:260
#, kde-format
msgid "File System"
msgstr "Sistem File"

#: soldevicetypes.cpp:263
#, kde-format
msgid "Partition Table"
msgstr "Tabel Partisi"

#: soldevicetypes.cpp:266
#, kde-format
msgid "Raid"
msgstr "Raid"

#: soldevicetypes.cpp:269
#, kde-format
msgid "Encrypted"
msgstr "Terenkripsi"

#: soldevicetypes.cpp:272
#, kde-format
msgctxt "unknown volume usage"
msgid "Unknown"
msgstr "Tak Diketahui"

#: soldevicetypes.cpp:275
#, kde-format
msgid "File System Type: "
msgstr "Tipe Sistem File: "

#: soldevicetypes.cpp:275
#, kde-format
msgid "Label: "
msgstr "Label: "

#: soldevicetypes.cpp:276
#, kde-format
msgid "Not Set"
msgstr "Tak Diatur"

#: soldevicetypes.cpp:276
#, kde-format
msgid "Volume Usage: "
msgstr "Penggunaan Volume: "

#: soldevicetypes.cpp:276
#, kde-format
msgid "UUID: "
msgstr "UUID: "

#: soldevicetypes.cpp:282
#, kde-format
msgid "Mounted At: "
msgstr "Dikaitkan Di: "

#: soldevicetypes.cpp:282
#, kde-format
msgid "Not Mounted"
msgstr "Tak Dikaitkan"

#: soldevicetypes.cpp:287
#, kde-format
msgid "Volume Space:"
msgstr "Ukuran Volume:"

#: soldevicetypes.cpp:297
#, kde-format
msgctxt "Available space out of total partition size (percent used)"
msgid "%1 free of %2 (%3% used)"
msgstr "%1 bebas dari %2 (%3% digunakan)"

#: soldevicetypes.cpp:303
#, kde-format
msgid "No data available"
msgstr "Tidak ada data tersedia"

#: soldevicetypes.cpp:330
#, kde-format
msgid "Multimedia Players"
msgstr "Pemutar Multimedia"

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Drivers: "
msgstr "Perangkat Didukung: "

#: soldevicetypes.cpp:349 soldevicetypes.cpp:388
#, kde-format
msgid "Supported Protocols: "
msgstr "Protokol Didukung: "

#: soldevicetypes.cpp:369
#, kde-format
msgid "Cameras"
msgstr "Kamera"

#: soldevicetypes.cpp:408
#, kde-format
msgid "Batteries"
msgstr "Baterai"

#: soldevicetypes.cpp:430
#, kde-format
msgid "PDA"
msgstr "PDA"

#: soldevicetypes.cpp:433
#, kde-format
msgid "UPS"
msgstr "UPS"

#: soldevicetypes.cpp:436
#, kde-format
msgid "Primary"
msgstr "Primer"

#: soldevicetypes.cpp:439
#, kde-format
msgid "Mouse"
msgstr "Mouse"

#: soldevicetypes.cpp:442
#, kde-format
msgid "Keyboard"
msgstr "Keyboard"

#: soldevicetypes.cpp:445
#, kde-format
msgid "Keyboard + Mouse"
msgstr "Keyboard + Mouse"

#: soldevicetypes.cpp:448
#, kde-format
msgid "Camera"
msgstr "Kamera"

#: soldevicetypes.cpp:451
#, kde-format
msgid "Phone"
msgstr "Telepon"

#: soldevicetypes.cpp:454
#, kde-format
msgctxt "Screen"
msgid "Monitor"
msgstr "Monitor"

#: soldevicetypes.cpp:457
#, kde-format
msgctxt "Wireless game pad or joystick battery"
msgid "Gaming Input"
msgstr "Input Gaming"

#: soldevicetypes.cpp:460
#, kde-format
msgctxt "unknown battery type"
msgid "Unknown"
msgstr "Tak diketahui"

#: soldevicetypes.cpp:466
#, kde-format
msgid "Charging"
msgstr "Mengecas"

#: soldevicetypes.cpp:469
#, kde-format
msgid "Discharging"
msgstr "Gak Ngecas"

#: soldevicetypes.cpp:472
#, kde-format
msgid "Fully Charged"
msgstr "Tercas Penuh"

#: soldevicetypes.cpp:475
#, kde-format
msgid "No Charge"
msgstr "Tidak Mengecas"

#: soldevicetypes.cpp:478
#, kde-format
msgid "Battery Type: "
msgstr "Tipe Baterai: "

#: soldevicetypes.cpp:478
#, kde-format
msgid "Charge Status: "
msgstr "Status Pengecasan: "

#: soldevicetypes.cpp:478
#, kde-format
msgid "Charge Percent: "
msgstr "Status Pengecasan: "

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Andhika Padmawan,Wantoyo"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "andhika.padmawan@gmail.com,wantoyek@gmail.com"

#~ msgid "kcmdevinfo"
#~ msgstr "kcmdevinfo"

#~ msgid "Device Viewer"
#~ msgstr "Penampil Peranti"

#~ msgid "(c) 2010 David Hubner"
#~ msgstr "(c) 2010 David Hubner"

#~ msgid "Network Interfaces"
#~ msgstr "Antarmuka Jaringan"

#~ msgid "Connected"
#~ msgstr "Tersambung"

#~ msgid "Wireless"
#~ msgstr "Nirkabel"

#~ msgid "Wired"
#~ msgstr "Kabel"

#~ msgid "Hardware Address: "
#~ msgstr "Alamat Peranti Keras: "

#~ msgid "Wireless?"
#~ msgstr "Nirkabel?"

#~ msgid "Audio Interfaces"
#~ msgstr "Antarmuka Audio"

#~ msgid "Alsa Interfaces"
#~ msgstr "Antarmuka Alsa"

#~ msgid "Open Sound System Interfaces"
#~ msgstr "Antarmuka Open Sound System"

#~ msgid "Control"
#~ msgstr "Kontrol"

#~ msgid "Input"
#~ msgstr "Masukan"

#~ msgid "Output"
#~ msgstr "Keluaran"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown audio interface type"
#~ msgid "Unknown"
#~ msgstr "Tak Diketahui"

#~ msgid "Internal Soundcard"
#~ msgstr "Kartu Suara Internal"

#~ msgid "USB Soundcard"
#~ msgstr "Kartu Suara USB"

#~ msgid "Firewire Soundcard"
#~ msgstr "Kartu Suara Firewire"

#~ msgid "Headset"
#~ msgstr "Headset"

#~ msgid "Modem"
#~ msgstr "Modem"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown sound card type"
#~ msgid "Unknown"
#~ msgstr "Tak Diketahui"

#~ msgid "Audio Interface Type: "
#~ msgstr "Tipe Antarmuka Audio: "

#~ msgid "Soundcard Type: "
#~ msgstr "Tipe Kartu Suara: "

#~ msgid "Device Buttons"
#~ msgstr "Tombol Divais"

#~ msgid "Lid Button"
#~ msgstr "Tombol Tutup"

#~ msgid "Power Button"
#~ msgstr "Tombol Daya"

#~ msgid "Sleep Button"
#~ msgstr "Tombol Tidur"

#~ msgid "Tablet Button"
#~ msgstr "Tombol Tablet"

#~ msgid "Unknown Button"
#~ msgstr "Tombol Tak Diketahui"

#~ msgid "Button type: "
#~ msgstr "Tipe tombol: "

#~ msgid "Has State?"
#~ msgstr "Punya Kondisi?"

#~ msgid "AC Adapters"
#~ msgstr "Adaptor AC"

#~ msgid "Is plugged in?"
#~ msgstr "Apakah ditancapkan?"

#~ msgid "Digital Video Broadcasting Devices"
#~ msgstr "Divais Penyiaran Video Digital"

#~ msgid "Audio"
#~ msgstr "Audio"

#~ msgid "Conditional access system"
#~ msgstr "Sistem akses kondisional"

#~ msgid "Demux"
#~ msgstr "Demux"

#~ msgid "Digital video recorder"
#~ msgstr "Perekam video digital"

#~ msgid "Front end"
#~ msgstr "Ujung depan"

#~ msgid "Network"
#~ msgstr "Jaringan"

#~ msgid "On-Screen display"
#~ msgstr "Tampilan Di-Layar"

#~ msgid "Security and content protection"
#~ msgstr "Keamanan dan perlindungan konten"

#~ msgid "Video"
#~ msgstr "Video"

#~ msgid "Device Type: "
#~ msgstr "Tipe Divais: "

#~ msgid "Serial Devices"
#~ msgstr "Divais Serial"

#, fuzzy
#~| msgid "Platform"
#~ msgctxt "platform serial interface type"
#~ msgid "Platform"
#~ msgstr "Platform"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown serial interface type"
#~ msgid "Unknown"
#~ msgstr "Tak Diketahui"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown port"
#~ msgid "Unknown"
#~ msgstr "Tak Diketahui"

#~ msgid "Serial Type: "
#~ msgstr "Tipe Serial: "

#~ msgid "Port: "
#~ msgstr "Pangkalan: "

#~ msgid "Smart Card Devices"
#~ msgstr "Divais Kartu Pintar"

#~ msgid "Card Reader"
#~ msgstr "Pembaca Kartu"

#~ msgid "Crypto Token"
#~ msgstr "Token Kripto"

#, fuzzy
#~| msgid "Unknown"
#~ msgctxt "unknown smart card type"
#~ msgid "Unknown"
#~ msgstr "Tak Diketahui"

#~ msgid "Smart Card Type: "
#~ msgstr "Tipe Kartu Pintar: "

#~ msgid "Video Devices"
#~ msgstr "Divais Video"

#~ msgid "Device unable to be cast to correct device"
#~ msgstr "Divais tak dapat dikaitkan ke divais yang benar"

#~ msgid "Percentage Used / Available: "
#~ msgstr "Persentase Digunakan / Tersedia: "
